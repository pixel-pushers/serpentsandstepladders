package main.model;

/** ----------------------------------------------------------------------------------------------
 * Ladder Class
 *      only contains variables and getters for begin space and end space
 *
 * Note:
 *      No logic or extra methods required as
 *      textBased.Model class is a better candidate to contain a method to move players piece up the ladders
 *      Rationale: textBased.Model will contain both the ladder objects to reference location
 *      and a player object to check and move piece location against ladder locations
 ---------------------------------------------------------------------------------------------- */
public class Ladder {

    private int beginSpace;
    private int endSpace;

    //Ladder Constructor
    // creates a new ladder object with values for begin space and end space
    // implemented as so:
    // Ladder ladderObject1 = new Ladder(1,100)
    public Ladder(int beginSpace, int endSpace) {
        this.beginSpace = beginSpace;
        this.endSpace = endSpace;
    }

    public boolean hasPLayerOnIt(int playerPosition){
        if (playerPosition == beginSpace){
            return true;
        }
        else return false;
    }

    public int getEndSpace() {
        return endSpace;
    }
}
