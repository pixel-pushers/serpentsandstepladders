package main.model;


import javafx.scene.paint.Color;

import java.util.ArrayList;

@SuppressWarnings("Duplicates")
public class Game {

    //fields

    //swing code
/*	private JFrame frame;*/

    private int currentPlayerNumber;
    private ArrayList<Player> players = new ArrayList<>(); // replaced array. this is same as array but has add method to make it easier.
    private Dice dice;
    private boolean isGameWon;
    private String gameWonBy;

    //constructor
    public Game() {

        isGameWon = false;

        dice = new Dice();
        currentPlayerNumber = 0;
    }

    //not sure if this is needed
    public ArrayList<Player> getPlayers() { return players; }

    public void addPlayer(String name, Color colour, boolean isCPU) {
        this.players.add(new Player(name,colour, isCPU)); // adds a new player to array
    }

    public boolean currentPlayerisCPU() {
        return this.players.get(currentPlayerNumber).isCPU();
    }

    //play method, this will itterate through turn
    // TODO replace with triggers from the view to progress through stages of the turn
    public void play(){
        rollDice();
        movePieceTo();
        checkClimb();
        checkfall();
        endTurn();

    }//end of play method

    public void rollDice() { this.dice.roll(); }

    // dice methods required by view.
    public int getDice1() { return this.dice.getDice1Value(); }
    public int getDice2() { return this.dice.getDice2Value(); }
    public boolean hasExtraTurn() { return this.dice.isDoubleRoll(); }

    public int movePieceTo() {
        int currentPosition = players.get(currentPlayerNumber).getLocator().getPosition();
        int totalMoves = this.dice.getTotalDiceValue();
        int proposedPosition = currentPosition+totalMoves;
        if (proposedPosition <= Locator.WINNING_POSITION)
            players.get(currentPlayerNumber).setLocator(currentPosition+totalMoves);
        //System.out.println(players.get(currentPlayerNumber).getLocator().getPosition());
        return players.get(currentPlayerNumber).getLocator().getPosition();
    }

    //move piece required by view
    public int getCurrentPlayerNumber() { return currentPlayerNumber;}


    public boolean checkClimb() {
        return this.getPlayers().get(currentPlayerNumber).getLocator().checkClimb();
    }

    public boolean checkfall() {
        return this.getPlayers().get(currentPlayerNumber).getLocator().checkFall();
    }

    public void endTurn(){
        if (players.get(currentPlayerNumber).getLocator().getPosition()
                == Locator.WINNING_POSITION){
            isGameWon = true;
            gameWonBy = players.get(currentPlayerNumber).getName();
        }

        //if isDoubleroll currentplayer remains unchanged, triggering extra turn
        if (!hasExtraTurn()){

            if (currentPlayerNumber < (players.size()-1))
                currentPlayerNumber++;
            else
                currentPlayerNumber = 0;
        }

    }

    public boolean isGameWon() {
        return isGameWon;
    }

    public String getGameWonBy() {
        return gameWonBy;
    }
}//end of class
