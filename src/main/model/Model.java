package main.model;

/**
 * Created by j on 25/03/17.
 * Modified 5/04/2017 by Nick

 * observable methods not implemented in initial stages
 * view will manually retrieve data with getters

 */
public class Model{

    Game game;

    //constructor
    public Model() {
        startGame();
    }

    public void startGame() {
        this.game = new Game();
        // TODO exit game conditions go here
    }

    public Game getGame() {
        return game;
    }

    public void resetGame() {
        this.game = new Game();
    }
}
