package main.model;

import main.data.SNAKES_LADDERS;

import java.util.ArrayList;

public class Locator {
	//fields
	public static final int WINNING_POSITION = 100;
	public static final int STARTING_POSITION = 0;

	private int position; //position 0-100 on board
	private ArrayList<Snake> snakes;
	private ArrayList<Ladder> ladders;


	//constructor
	public Locator() {
		position = STARTING_POSITION;
		snakes = new ArrayList<>();
		ladders = new ArrayList<>();
		// USING TEST SNAKE AND LADDER LOCATIONS TO TEST MODEL

		addSnakeLadders();
	}

	//getters, setters
	public int getPosition() {return position; }
	public void setPosition(int p) { this.position = p; }


	public boolean checkClimb(){
		for (int i = 0; i < ladders.size() ; i++) {
			if (ladders.get(i).hasPLayerOnIt(position)) {
				position = ladders.get(i).getEndSpace();
				return true;
			}
		}
		return false;
	}//end of ladder method


	public boolean checkFall() {
		for (int i = 0; i < snakes.size() ; i++) {
			if (snakes.get(i).hasPLayerOnIt(position)) {
				position = snakes.get(i).getEndSpace();
				return true;
			}
		}
		return false;
	}//end of fall method


	public void addSnakeLadders() {
		snakes.add(SNAKES_LADDERS.SNAKE1);
		snakes.add(SNAKES_LADDERS.SNAKE2);
		snakes.add(SNAKES_LADDERS.SNAKE3);
		snakes.add(SNAKES_LADDERS.SNAKE4);
		snakes.add(SNAKES_LADDERS.SNAKE5);

		ladders.add(SNAKES_LADDERS.LADDER1);
		ladders.add(SNAKES_LADDERS.LADDER2);
		ladders.add(SNAKES_LADDERS.LADDER3);
		ladders.add(SNAKES_LADDERS.LADDER4);
		ladders.add(SNAKES_LADDERS.LADDER5);

	}

}
