package main.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.text.Text;
import main.controller.AppController;


public class WinViewController{

    public AppController appController;
    @FXML
    public Group group = new Group();

    @FXML
    private Text DisplayWinner;



    public void setDisplayWinner() {
        DisplayWinner.setText(appController.getGameWonBy());
    }
    @FXML      //executed when Exit To menu button pushed in win screen
    private void FinishGame(ActionEvent event){
        appController.loadMenuView();
    }

    public void setAppController(AppController appController) {
        //executed after this controller is created
        this.appController = appController;
    }

}
