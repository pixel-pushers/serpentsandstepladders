package main.application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import main.controller.AppController;
import main.model.Model;
import main.view.BoardViewController;
import main.view.MenuViewController;
import main.view.PlayerInputViewController;
import main.view.WinViewController;

import java.io.IOException;

/**
 *
 */
@SuppressWarnings("Duplicates")
public class app extends Application {

    Model model = new Model();
    AppController appController = new AppController(model, this);

    BorderPane root = new BorderPane();

    @Override
    public void start(Stage primaryStage) throws IOException {

        loadMenuView();

        primaryStage.setTitle("Snakes & StepLadders");
        primaryStage.setScene(new Scene(root, 1280, 720));
        primaryStage.show();

    }

    public void loadMenuView(){
        FXMLLoader viewLoader = new FXMLLoader(getClass().getResource("/main/view/MenuView.fxml"));
        try {
            root.setCenter(viewLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        MenuViewController viewController = viewLoader.getController();
        viewController.setAppController(appController);
    }

    public void loadPlayerInputView(){
        FXMLLoader viewLoader = new FXMLLoader(getClass().getResource("/main/view/PlayerInputView.fxml"));
        try {
            root.setCenter(viewLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        PlayerInputViewController viewController = viewLoader.getController();
        viewController.setAppController(appController);
    }

    public void loadBoardView(){
        FXMLLoader viewLoader = new FXMLLoader(getClass().getResource("/main/view/BoardView.fxml"));
        try {
            root.setCenter(viewLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        BoardViewController viewController = viewLoader.getController();
        viewController.setAppController(appController);
        viewController.setNames();
        viewController.displayPlayerTurnInfo();
        viewController.setPlayerColours();
    }

    public void loadWinView(){
        FXMLLoader viewLoader = new FXMLLoader(getClass().getResource("/main/view/WinView.fxml"));
        try {
            root.setCenter(viewLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        WinViewController viewController = viewLoader.getController();
        viewController.setAppController(appController);
        viewController.setDisplayWinner();
    }

    public void closeApp(){
        System.exit(0);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}