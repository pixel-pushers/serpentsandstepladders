package main.model;

/**
 * Created by Sam, Nic and Jason
 */
public class Dice {

    private int dice1Value; //value of first dice to be pulled from the first roll
    private int dice2Value; // value of second dice to be pulled from the first roll
    private int totalDiceValue; // value of all rolls together finalised/
    private boolean isDoubleRoll;

    // constructor
    public Dice() {
        this.roll();
    }


    //roll method
    public void roll(){
        dice1Value = (int) (Math.random() * 6) + 1;
        dice2Value = (int) (Math.random() * 6) + 1;

        if (dice1Value == dice2Value)
            isDoubleRoll = true;
        else
            isDoubleRoll = false;

        totalDiceValue = dice1Value + dice2Value;
    }

    public boolean isDoubleRoll() { return isDoubleRoll; }

    public int getDice1Value() { return dice1Value; }

    public int getDice2Value() { return dice2Value; }

    public int getTotalDiceValue() { return totalDiceValue; }

}
