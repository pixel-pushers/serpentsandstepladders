package main.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import main.controller.AppController;

import java.io.IOException;

/**
 * Created by j on 10/05/17.
 */
public class MenuViewController{

    public AppController appController;
    @FXML
    public Group group = new Group();


    //TODO move-> MenuView
    @FXML      //Executed when Start game button pushed in menuView
    private void StartGame(ActionEvent event) throws IOException {
        appController.loadPlayerInputView();
    }


    @FXML     //Executed when exit game button pushed in menuView
    private void exitView(ActionEvent event) throws IOException {
        appController.exitApp();

    }

    public void setAppController(AppController appController) {
        //executed after this controller is created
        this.appController = appController;
    }

}
