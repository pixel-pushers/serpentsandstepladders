package main.view;

import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import main.controller.AppController;

/**
 * Created by j on 10/05/17.
 */
@SuppressWarnings("Duplicates")
public class BoardViewController {



    public AppController appController;

    @FXML
    public Group group = new Group();


    // Circle player pieces - JavaFX circle objects
    @FXML
    private Circle player1 = new Circle();
    @FXML
    private Circle player2 = new Circle();
    @FXML
    private Circle player3 = new Circle();
    @FXML
    private Circle player4 = new Circle();

    //snakes images- JavaFX ImageView objects
    @FXML
    private ImageView snake1 = new ImageView();
    @FXML
    private ImageView snake2 = new ImageView();
    @FXML
    private ImageView snake3 = new ImageView();
    @FXML
    private ImageView snake4 = new ImageView();
    @FXML
    private ImageView snake5 = new ImageView();

    //ladders images- JavaFX ImageView objects
    @FXML
    private ImageView ladder1 = new ImageView();
    @FXML
    private ImageView ladder2 = new ImageView();
    @FXML
    private ImageView ladder3 = new ImageView();
    @FXML
    private ImageView ladder4 = new ImageView();
    @FXML
    private ImageView ladder5 = new ImageView();

    //dice number images
    @FXML
    public ImageView diceOneDis;
    @FXML
    public ImageView diceTwoDis;

    //info box text
    @FXML
    private Text playerOne;
    @FXML
    private Text playerTwo;
    @FXML
    private Text playerThree;
    @FXML
    private Text playerFour;
    @FXML
    private Text player1Turn;
    @FXML
    private Text player2Turn;
    @FXML
    private Text player3Turn;
    @FXML
    private Text player4Turn;

    public void setPlayerColours(){
        int len = appController.getPlayerCount();
        if(len == 1){
            player1.setFill(appController.getPlayerColour(0));
            player1.setStroke(Color.BLACK);
        }else if(len == 2){
            player1.setFill(appController.getPlayerColour(0));
            player1.setStroke(Color.BLACK);
            player2.setFill(appController.getPlayerColour(1));
            player2.setStroke(Color.BLACK);
        }else if(len == 3){
            player1.setFill(appController.getPlayerColour(0));
            player1.setStroke(Color.BLACK);
            player2.setFill(appController.getPlayerColour(1));
            player2.setStroke(Color.BLACK);
            player3.setFill(appController.getPlayerColour(2));
            player3.setStroke(Color.BLACK);
        }else if(len == 4){
            player1.setFill(appController.getPlayerColour(0));
            player1.setStroke(Color.BLACK);
            player2.setFill(appController.getPlayerColour(1));
            player2.setStroke(Color.BLACK);
            player3.setFill(appController.getPlayerColour(2));
            player3.setStroke(Color.BLACK);
            player4.setFill(appController.getPlayerColour(3));
            player4.setStroke(Color.BLACK);

        }
//        player1.setFill(appController.getPlayerColour(0));
//        player1.setStroke(Color.BLACK);
//        player2.setFill(appController.getPlayerColour(1));
//        player2.setStroke(Color.BLACK);
//        player3.setFill(appController.getPlayerColour(2));
//        player3.setStroke(Color.BLACK);
//        player4.setFill(appController.getPlayerColour(3));
//        player4.setStroke(Color.BLACK);
    }


    @FXML       //executed when Exit to menu button pushed in board view
    private void HandleSubmitAction(ActionEvent event){
        appController.loadMenuView();
    }

    @FXML   //executed when dice image clicked in board screen - Roll dice and display dice
    private void rollDice(ActionEvent event){

        if (!checkWinGame()) {
            completeTurn();
            if (!checkWinGame())
                checkTurnCPU();
        }
    }


    public void updateDice() {

        int dice1 = appController.getDice1Value();
        int dice2 = appController.getDice2Value();

        ////dice 1 calc
        if (dice1 == 1) {
            diceOneDis.setImage(new Image("main/view/BoardImages/one.png"));
        } else if (dice1 == 2) {
            diceOneDis.setImage(new Image("main/view/BoardImages/two.png"));
        } else if (dice1 == 3) {
            diceOneDis.setImage(new Image("main/view/BoardImages/three.png"));
        } else if (dice1 == 4) {
            diceOneDis.setImage(new Image("main/view/BoardImages/four.png"));
        } else if (dice1 == 5) {
            diceOneDis.setImage(new Image("main/view/BoardImages/five.png"));
        } else if (dice1 == 6) {
            diceOneDis.setImage(new Image("main/view/BoardImages/six.png"));
        }

        //dice 2 calc
        if (dice2 == 1) {
            diceTwoDis.setImage(new Image("main/view/BoardImages/one.png"));
        } else if (dice2 == 2) {
            diceTwoDis.setImage(new Image("main/view/BoardImages/two.png"));
        } else if (dice2 == 3) {
            diceTwoDis.setImage(new Image("main/view/BoardImages/three.png"));
        } else if (dice2 == 4) {
            diceTwoDis.setImage(new Image("main/view/BoardImages/four.png"));
        } else if (dice2 == 5) {
            diceTwoDis.setImage(new Image("main/view/BoardImages/five.png"));
        } else if (dice2 == 6) {
            diceTwoDis.setImage(new Image("main/view/BoardImages/six.png"));
        }
    }
    //movement methods

    // used in movePlayerPiece method
    private int calculateXLocation(int location){
        int[] boardCordsX = {84,140,200,260,320,380,440,500,560,620};
        int[] boardCordsXInverted = {620,560,500,440,380,320,260,200,140,84};
        int xPos = (location % 10);
        int yItereation = (location - (location % 10)) / 10;
        if ((yItereation%2)!=1)
        {
            return (boardCordsX[xPos]);
        }
        else
        {
            return (boardCordsXInverted[xPos]);
        }
    }

    // used in movePlayerPiece method
    private int calculateYLocation(int location) {
        int[] boardCordsY = {596, 540, 484, 428, 372, 316, 260, 204, 148, 92};
        int yPos = (location - (location % 10)) / 10;
        return(boardCordsY[yPos]);

    }

    //moves player piece on the board
    public void movePlayerPiece(int playerNum, int location)
    {
        int rad = 20;
       // System.out.println(location);
        for (int i = 0; i < appController.getPlayerCount(); i++)
        {
       // System.out.println("player " + i + "location = "+appController.getPlayerLocation(i));

            if (appController.getPlayerLocation(i) - 1 == location){
                if (i != playerNum){
                    rad = 15;
                }

            }
        }
        int xPos = calculateXLocation(location);
        int yPos = calculateYLocation(location);
        if(playerNum == 0)
        {
            group.getChildren().remove(player1);
            player1.setCenterX(xPos);
            player1.setCenterY(yPos);
            player1.setRadius(rad);
            group.getChildren().add(player1);
        }

        if(playerNum == 1)
        {
            group.getChildren().remove(player2);
            player2.setCenterX(xPos);
            player2.setCenterY(yPos);
            player2.setRadius(rad);
            group.getChildren().add(player2);
        }
        if(playerNum == 2)
        {
            group.getChildren().remove(player3);
            player3.setCenterX(xPos);
            player3.setCenterY(yPos);
            player3.setRadius(rad);
            group.getChildren().add(player3);
        }
        if(playerNum == 3)
        {
            group.getChildren().remove(player4);
            player4.setCenterX(xPos);
            player4.setCenterY(yPos);
            player4.setRadius(rad);
            group.getChildren().add(player4);
        }



    }

    // -----------------------------------------------------------------------------

    // turn progression methods



    //Will be triggered in Game and will display current player
    //number or current player number followed by rolled doubles message in view.
    public void displayPlayerTurnInfo() {

        String extraTurnMsg = "You rolled doubles, have an extra turn";
        String yourTurnMsg = "It's your turn";
        String waitingTurnMsg = "Waiting for turn...";
        int len = appController.getPlayerCount();

        if (appController.getCurrentPlayerNumber() == 0 ) {
            if (appController.extraTurn()) {
                player1Turn.setText(extraTurnMsg);
            } else {
                player1Turn.setText(yourTurnMsg);
            }
            if(len == 2){
                player2Turn.setText(waitingTurnMsg);
            }else if(len == 3){
                player2Turn.setText(waitingTurnMsg);
                player3Turn.setText(waitingTurnMsg);
            }else if(len == 4){
                player2Turn.setText(waitingTurnMsg);
                player3Turn.setText(waitingTurnMsg);
                player4Turn.setText(waitingTurnMsg);
            }


        }

        else if (appController.getCurrentPlayerNumber() == 1 ) {
            if (appController.extraTurn()) {
                player2Turn.setText(extraTurnMsg);
            } else {
                player2Turn.setText(yourTurnMsg);
            }
            player1Turn.setText(waitingTurnMsg);
            if(len == 3){
                player3Turn.setText(waitingTurnMsg);
            }else if(len == 4) {
                player3Turn.setText(waitingTurnMsg);
                player4Turn.setText(waitingTurnMsg);
            }



        }

        else if (appController.getCurrentPlayerNumber() == 2 ) {
            if (appController.extraTurn()) {
                player3Turn.setText(extraTurnMsg);
            } else {
                player3Turn.setText(yourTurnMsg);
            }
            player1Turn.setText(waitingTurnMsg);
            player2Turn.setText(waitingTurnMsg);
            if(len == 4){
                player4Turn.setText(waitingTurnMsg);
            }



        }

        else if (appController.getCurrentPlayerNumber() == 3) {
            if (appController.extraTurn()) {
                player4Turn.setText(extraTurnMsg);
            } else {
                player4Turn.setText(yourTurnMsg);
            }

            player1Turn.setText(waitingTurnMsg);
            player2Turn.setText(waitingTurnMsg);
            player3Turn.setText(waitingTurnMsg);

        }

    }

    public void setNames(){
        int len = appController.getPlayerCount();
        if(len == 1){
            playerOne.setText(appController.getPlayerName(0));
        }else if(len == 2){
            playerOne.setText(appController.getPlayerName(0));
            playerTwo.setText(appController.getPlayerName(1));
        }else if(len == 3){
            playerOne.setText(appController.getPlayerName(0));
            playerTwo.setText(appController.getPlayerName(1));
            playerThree.setText(appController.getPlayerName(2));
        }else if(len == 4){
            playerOne.setText(appController.getPlayerName(0));
            playerTwo.setText(appController.getPlayerName(1));
            playerThree.setText(appController.getPlayerName(2));
            playerFour.setText(appController.getPlayerName(3));
        }
}

    // -----------------------------------------------------------------------------

    //snakes and ladders methods



    public int placeSnake(int location, int snakeType)
    {
        /*Will be triggered on Game side upon the start of a game and will update view with a placed snake
        in the location given (Will originally use static values.)
         */


        //HAVE TO DESIGN SNAKES WITH CENTRE ON HEAD
        //have to implement image urls



        if(snakeType == 1){
            snake1.setImage(new Image("main/view/BoardImages/Snakes/s1.png"));
            snake1.setX(calculateXLocation(location)-300);
            snake1.setY(calculateYLocation(location)-300);
            group.getChildren().add(snake1);
            return calculateTail(location, -5, -3);

        }
        else if(snakeType == 2){
            snake2.setImage(new Image("main/view/BoardImages/Snakes/s2.png"));
            snake2.setX(calculateXLocation(location)-300);
            snake2.setY(calculateYLocation(location)-300);
            group.getChildren().add(snake2);
            return calculateTail(location, 3, -5);

        }
        else if(snakeType == 3){
            snake3.setImage(new Image("main/view/BoardImages/Snakes/s3.png"));
            snake3.setX(calculateXLocation(location)-300);
            snake3.setY(calculateYLocation(location)-300);
            group.getChildren().add(snake3);
            return calculateTail(location, -2, -4);

        }
        else if(snakeType == 4){
            snake4.setImage(new Image("main/view/BoardImages/Snakes/s4.png"));
            snake4.setX(calculateXLocation(location)-300);
            snake4.setY(calculateYLocation(location)-300);
            group.getChildren().add(snake4);
            return calculateTail(location, 2, -5);

        }
        else {
            snake5.setImage(new Image("main/view/BoardImages/Snakes/s5.png"));
            snake5.setX(calculateXLocation(location)-300);
            snake5.setY(calculateYLocation(location)-300);
            group.getChildren().add(snake5);
            return calculateTail(location, -3, -3);

        }

    }

    public int calculateTail(int location,int xDif, int yDif) {
        //There has to be a simpler way of working this out, but I've done this already, and it SHOULD work
        int xLoc = calculateXLocation(location);
        int xNum = 0;
        int[] boardCordsX = {84, 140, 200, 260, 320, 380, 440, 500, 560, 620};
        int[] boardCordsY = {596, 540, 484, 428, 372, 316, 260, 204, 148, 92};
        for (int i = 0; i < 10; i++) {
            if (xLoc == boardCordsX[i]) {
                xNum = i;
            }
        }
        int tailLocX = xNum + xDif;
        if (tailLocX >= 10 ^ tailLocX < 0) {
            return 101;
        }
        int yLoc = calculateYLocation(location);
        int yNum = 0;
        for (int i = 0; i < 10; i++) {
            if (yLoc == boardCordsY[i]) {
                yNum = i;
            }
        }
        int tailLocY = yNum + yDif;
        if (tailLocY >= 10 ^ tailLocY < 0) {
            return 101;
        }
        int finalLoc = tailLocX + (tailLocY * 10);
        return finalLoc;
    }



    public int placeLadder(int location, int ladderType)
    {
        /*Will be triggered on Game side upon the start of a game and will update view with a placed ladder
        in the location given (Will originally use static values.)
         */
        //LOCATION INPUT IS TO BE FOR THE TOP OF THE LADDER

        if(ladderType == 1) {
            ladder1.setImage(new Image("main/view/BoardImages/Ladders/l1.png"));
            ladder1.setX(calculateXLocation(location) - 300);
            ladder1.setY(calculateYLocation(location) - 300);
            group.getChildren().add(ladder1);
            return calculateTail(location, -2, -5);
        }
        if(ladderType == 2) {
            ladder2.setImage(new Image("main/view/BoardImages/Ladders/l2.png"));
            ladder2.setX(calculateXLocation(location) - 300);
            ladder2.setY(calculateYLocation(location) - 300);
            group.getChildren().add(ladder2);
            return calculateTail(location, 2, -4);
        }
        if(ladderType == 3) {
            ladder3.setImage(new Image("main/view/BoardImages/Ladders/l3.png"));
            ladder3.setX(calculateXLocation(location) - 300);
            ladder3.setY(calculateYLocation(location) - 300);
            group.getChildren().add(ladder3);
            return calculateTail(location, 0, -5);
        }
        if(ladderType == 4) {
            ladder4.setImage(new Image("main/view/BoardImages/Ladders/l4.png"));
            ladder4.setX(calculateXLocation(location) - 300);
            ladder4.setY(calculateYLocation(location) - 300);
            group.getChildren().add(ladder4);
            return calculateTail(location, 2, -2);
        }
        else {
            ladder5.setImage(new Image("main/view/BoardImages/Ladders/l5.png"));
            ladder5.setX(calculateXLocation(location) - 300);
            ladder5.setY(calculateYLocation(location) - 300);
            group.getChildren().add(ladder5);
            return calculateTail(location, -3, -3);
        }











    }

    // -----------------------------------------------------------------------------

    //win screen methods

    public boolean checkWinGame()
    {
        if (appController.isGameWon()) {
            appController.loadWinView();
            return true;
        }
        else return false;
    }

    public void checkTurnCPU(){

        PauseTransition pause = new PauseTransition(javafx.util.Duration.millis(1500));
        pause.setOnFinished(event -> {
            if (appController.currentPlayerIsCPU()) {
                completeTurn();
                if (!checkWinGame())
                    checkTurnCPU();
            }
        });
        pause.play();

    }



    public void completeTurn() {

        appController.rollDice();
        updateDice();



        movePlayerPiece(appController.getCurrentPlayerNumber(), appController.movePlayer() - 1);

        if (appController.checkClimb()) {
            //add in some way to set a message stating player has climbed ladder
            movePlayerPiece(appController.getCurrentPlayerNumber(), appController.getCurrentPlayerLocation() - 1);
        } else if (appController.checkFall()) {
            //add in some way to set a message stating player has climbed ladder
            movePlayerPiece(appController.getCurrentPlayerNumber(), appController.getCurrentPlayerLocation() - 1);
        }
        appController.endTurn();
        this.displayPlayerTurnInfo();

    }

    public void setAppController(AppController appController) {
        //executed after this controller is created
        this.appController = appController;
    }

}
