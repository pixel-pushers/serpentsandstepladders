package main.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import main.controller.AppController;

import java.io.IOException;

@SuppressWarnings("Duplicates")
public class PlayerInputViewController{

    public AppController appController;
    @FXML
    public Group group = new Group();


    //player input
    @FXML
    private TextField player1Name;
    @FXML
    private TextField player2Name;
    @FXML
    private TextField player3Name;
    @FXML
    private TextField player4Name;
    @FXML
    private ColorPicker player1Colour;
    @FXML
    private ColorPicker player2Colour;
    @FXML
    private ColorPicker player3Colour;
    @FXML
    private ColorPicker player4Colour;
    @FXML
    private CheckBox addCPUPlayer1;
    @FXML
    private CheckBox addCPUPlayer2;
    @FXML
    private CheckBox addCPUPlayer3;
    @FXML
    private CheckBox addCPUPlayer4;

    @FXML       //executed when done button pressed on playerAmountScreen
    private void playerNamesDone(ActionEvent event) throws IOException {

        if(!player1Name.getText().isEmpty()){
            String name = player1Name.getText();
            Color color = player1Colour.getValue();
            Boolean isCPU = addCPUPlayer1.isSelected();
            appController.addPlayer(name,color,isCPU);
        }

        if(!player2Name.getText().isEmpty()){
            String name = player2Name.getText();
            Color color = player2Colour.getValue();
            Boolean isCPU = addCPUPlayer2.isSelected();
            appController.addPlayer(name,color,isCPU);
        }

        if(!player3Name.getText().isEmpty()){
            String name = player3Name.getText();
            Color color = player3Colour.getValue();
            Boolean isCPU = addCPUPlayer3.isSelected();
            appController.addPlayer(name,color,isCPU);
        }
        if(!player4Name.getText().isEmpty()){
            String name = player4Name.getText();
            Color color = player4Colour.getValue();
            Boolean isCPU = addCPUPlayer4.isSelected();
            appController.addPlayer(name,color,isCPU);
        }

        appController.loadBoardView();

    }

    public void setAppController(AppController appController) {
        //executed after this controller is created
        this.appController = appController;
    }
}
