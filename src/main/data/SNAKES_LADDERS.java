package main.data;

import main.model.Ladder;
import main.model.Snake;

/**
 * Created by j on 6/05/17.
 */
public class SNAKES_LADDERS {

    public static final Ladder LADDER1 = new Ladder(13,35) ;
    public static final Ladder LADDER2 = new Ladder(3,56);
    public static final Ladder LADDER3 = new Ladder(32,74);
    public static final Ladder LADDER4 = new Ladder(40,81);
    public static final Ladder LADDER5 = new Ladder(58,86);

    public static final Snake SNAKE1 = new Snake(99,45) ;
    public static final Snake SNAKE2 = new Snake(73,30);
    public static final Snake SNAKE3 = new Snake(46,22);
    public static final Snake SNAKE4 = new Snake(48,18);
    public static final Snake SNAKE5 = new Snake(89,55);


}
