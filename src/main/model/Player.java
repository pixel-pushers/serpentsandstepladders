package main.model;

import javafx.scene.paint.Color;

public class Player {

	//fields
	private String name;
	private Locator locator;
	private Color colour;
	private boolean isCPU;

	//constructor
	public Player(String name, Color colour, boolean isCPU){
		locator = new Locator();
		this.colour = colour;
		this.name = name;
		this.isCPU = isCPU;
	}

	public Player(String name, Color colour){
		locator = new Locator();
		this.colour = colour;
		this.name = name;
		this.isCPU = false;

	}
	//mutators
	public String getName() { return name; }

	public Locator getLocator() { return locator; }

	public void setLocator(int pos) {
		locator.setPosition(pos); }

	public Color getColour() { return colour; }

	public boolean isCPU() {
		return isCPU;
	}
}


