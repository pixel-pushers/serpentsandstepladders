package main.controller;

import javafx.scene.paint.Color;
import main.application.app;
import main.model.Model;

public class AppController {


    private Model model;
    private app app;

    public AppController(Model model, app app) {
        this.app = app;
        this.model = model;
    }

    public void loadMenuView(){
        app.loadMenuView();
        resetGame();
    }
    public void loadBoardView(){
        app.loadBoardView();
    }
    public void loadPlayerInputView(){
        app.loadPlayerInputView();
    }
    public void loadWinView(){
        app.loadWinView();
    }


    // --------------------------------------------------------------------------------------
    // general player methods

    //add a player to the games ArrayList
    // e.g. appController.addPlayer("Jason","Blue",false)
    // or ("CPU1","g",true)
    public void addPlayer(String name, Color colour, boolean isCPU){
        this.model.getGame().addPlayer(name, colour, isCPU);
        //System.out.println(model.getGame());
    }

    public int getPlayerCount(){
        return this.model.getGame().getPlayers().size();
    }

    public String getPlayerName(int playerNumber){
        //System.out.println(model.getGame());

        return this.model.getGame().getPlayers().get(playerNumber).getName();
    }

    public Color getPlayerColour(int playerNumber){
        return this.model.getGame().getPlayers().get(playerNumber).getColour();
    }

    public int getPlayerLocation(int playerNumber){
        return this.model.getGame().getPlayers().get(playerNumber).getLocator().getPosition();
    }

    // --------------------------------------------------------------------------------------
    // Current player methods ( for player with active turn )

    public String getCurrentPlayerName() {
        return this.model.getGame().getPlayers().get(getCurrentPlayerNumber()).getName();
    }

    public int getCurrentPlayerNumber(){
        return model.getGame().getCurrentPlayerNumber();
    }

    public int getCurrentPlayerLocation() { return model.getGame().getPlayers().get(getCurrentPlayerNumber()).getLocator().getPosition(); }


    public boolean currentPlayerIsCPU() {
        return this.model.getGame().currentPlayerisCPU();
    }

// ------------------------------------------------------------
    // dice methods

    public void rollDice() {
        this.model.getGame().rollDice();
    }

    public int getDice1Value() {
        return model.getGame().getDice1();
    }

    public int getDice2Value() {
        return model.getGame().getDice2();
    }

// ---------------------------------------------------------------------------
    // movement methods

    // changes players location position + diceroll
    // returns position after move
    public int movePlayer() {
        return this.model.getGame().movePieceTo();
    }


    // checks if player position is at bottom of ladder
    // if it is players location is changed to top of ladder and true returned
    // if its not false is returned players position remains the same
    public boolean checkClimb() { return this.model.getGame().checkClimb();}

    // checks if player position is at top of snake
    // if it is players location is changed to  bottom of snake and true returned
    // if its not false is returned players position remains the same
    public boolean checkFall() { return this.model.getGame().checkfall();}

    //----------------------------------------------------------
    // game state and turn methods

    //checks if player has extra turn
    public boolean extraTurn() {
        return model.getGame().hasExtraTurn();
    }

    //ends player turn changing to next player in ArrayList
    public void endTurn() {
        this.model.getGame().endTurn();
    }

    //triggered when player reaches position 100 - can be used to end game and send to win screen
    // TODO currently model set to win if position >= 100 rather than needing to be exactly 100 or reroll
    public boolean isGameWon(){
        return this.model.getGame().isGameWon();
    }

    public String getGameWonBy(){
        return model.getGame().getGameWonBy();
    }

    //start Game, initialize new model with new game
    // TODO add end game method
    public void startGame(){
        this.model.startGame();
    }

    public void exitApp(){
        this.app.closeApp();
    }

    public void resetGame(){
        this.model.resetGame();
    }

}