package main.model;

/**
 * Created by j on 29/04/17.
 */
public class Snake {

    private int beginSpace;
    private int endSpace;

    public Snake(int beginSpace, int endSpace) {
        this.beginSpace = beginSpace;
        this.endSpace = endSpace;
    }

    public boolean hasPLayerOnIt(int playerPosition){
        if (playerPosition == beginSpace){
            return true;
        }
        else return false;
    }


    public int getEndSpace() {
        return endSpace;
    }
}
